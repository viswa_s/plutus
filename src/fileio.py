import sys
# function for reading a file and returning a single string
def readmyfile(filename):
	if not filename:
		print("no input provided")
		sys.exit()
	try:
		myfile = open(filename,"r")
	except IOError:
		print('error opening the file', filename)
	else:
		lines = myfile.readlines();
		myfile.close()
		return lines

def readbyiter(filename):
	if not filename:
		print ("no input provided")
		sys.exit()
	try:
		for line in open(filename): print(line)
	except IOError:
		print('error opening the file',filename)
		

def readbyline(filename):
	if not filename:
		print ("no input provided")
		sys.exit()
	try:
		myfile = open(filename,"r")
	except IOError:
		print('Error opening file',filename)
	else:
		for line in myfile.readline():
			print(line)
			
def readall(filename):
	if not filename:
		print ("no input provided")
		sys.exit()
	try:
		myfile = open(filename,"r")
	except IOError:
		print('Error opening file',filename)
	else:
		line = myfile.read()
		#print(line)
		return line
"""
def writemyfile(myfile,lines):
	try:
		myfile
"""			

