import fileio
import os.path
import collections
"""
	Genesis: 12/08/2014
	This file contains the basic data types and their definition.
	This files holds the over all structures of our items.
"""
CONFIG_DIR = os.path.dirname(os.path.abspath('.')) + '/config' 
SECTORS_FILE = CONFIG_DIR + '/sectors.conf'
INDUSTRIES_FILE = CONFIG_DIR + '/industries.conf'
QUOTEPROPS_FILE = CONFIG_DIR + '/quoteprops.conf'

sectors = collections.namedtuple('sectors','name desc id')
industries = collections.namedtuple('industries','name id')
qprops = collections.namedtuple('qprops','name desc id')

def loadsectorconf():
	lines = fileio.readall(SECTORS_FILE)
	sectors_info = []
	for line in lines.split():
		tk = line.split(',')
		sectors_info.append(sectors(tk[0],tk[1],tk[2]))
	return sectors_info	

def loadindustryconf():
	lines = fileio.readall(INDUSTRIES_FILE)
	indus_info = []
	for line in lines.split():
		tk = line.split(',')
		indus_info.append(industries(tk[0],tk[1]))
	return indus_info	

def loadquotepropsconf():
	lines = fileio.readall(QUOTEPROPS_FILE)
	#lines = fileio.readmyfile(QUOTEPROPS_FILE)
	qprops_info = []
	for ln in lines.splitlines():
		#print(ln)
		tkn = ln.split(',')
		qprops_info.append(qprops(tkn[0],tkn[1],tkn[2]))
	return qprops_info	
